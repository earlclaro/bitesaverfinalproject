package com.example.finalproject.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.finalproject.Adapter.CartListAdapter;
import com.example.finalproject.Helper.ChangeNumberItemsListener;
import com.example.finalproject.Helper.ManagementCart;
import com.example.finalproject.R;

public class CartActivity extends AppCompatActivity {
private RecyclerView.Adapter adapter;
private RecyclerView recyclerViewList;
private ManagementCart managementCart;
private TextView totalFeeTxt,taxtTxt,deliveryTxt,totalTxt,emptyTxt;
private double tax;
private ScrollView scrollView;
private ImageView backBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

    managementCart=new ManagementCart(this);

    initView();
    initList();
    calculateCart();
    setVariable();
    }

    private void setVariable() {
        backBtn.setOnClickListener(view -> finish());
    }

    private void initList() {
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this,LinearLayoutManager.VERTICAL, false);
        recyclerViewList.setLayoutManager(linearLayoutManager);
        adapter=new CartListAdapter(managementCart.getListCart(), this, new ChangeNumberItemsListener() {
            @Override
            public void changed() {
        calculateCart();
            }
        });

        recyclerViewList.setAdapter(adapter);

        if (managementCart.getListCart().isEmpty()){
            emptyTxt.setVisibility(View.VISIBLE);
            scrollView.setVisibility(View.GONE);
        }else {
            emptyTxt.setVisibility(View.GONE);
            scrollView.setVisibility(View.VISIBLE);
        }


    }

    private void calculateCart(){
        double percentTax=0.02;
        double delivery = 10;
        tax=Math.round((managementCart.getTotalFee()*percentTax*100.0))/100.0 ;

        double total=Math.round((managementCart.getTotalFee()+tax+delivery)*100.0)/100;
        double itemTotal = Math.round(managementCart.getTotalFee() * 100.0) / 100.0;

       totalFeeTxt.setText("₱"+itemTotal);
        taxtTxt.setText("₱"+tax);
        deliveryTxt.setText("₱"+delivery);
        totalTxt.setText("₱"+total);
    }

    private void initView() {
        totalFeeTxt=findViewById(R.id.totalFeeTxt);
        taxtTxt=findViewById(R.id.taxTxt);
        deliveryTxt=findViewById(R.id.deliveryTxt);
        totalTxt=findViewById(R.id.totalTxt);
        recyclerViewList=findViewById(R.id.view3);
        scrollView=findViewById(R.id.scrollView);
        backBtn=findViewById(R.id.backBtn);
        emptyTxt=findViewById(R.id.emptyTxt);
    }
}