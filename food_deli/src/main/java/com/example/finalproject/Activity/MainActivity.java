package com.example.finalproject.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.example.finalproject.Adapter.FoodListAdapter;
import com.example.finalproject.Domain.FoodDomain;
import com.example.finalproject.R;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
private RecyclerView.Adapter adapterFoodList;
private RecyclerView recyclerViewFood;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initRecycleview();
        bottomNavigation();
    }

    private void bottomNavigation() {
        LinearLayout homeBtn=findViewById(R.id.homeBtn);
        LinearLayout cartBtn=findViewById(R.id.cartBtn);

        homeBtn.setOnClickListener(view -> startActivity(new Intent(MainActivity.this,MainActivity.class)));

        cartBtn.setOnClickListener(view -> startActivity(new Intent(MainActivity.this,CartActivity.class)));
    }

    private void initRecycleview() {
        ArrayList<FoodDomain> items = new ArrayList<>();
        items.add(new FoodDomain("Cheese Burger","Satisfy your cravings with our juicy Cheese Burger. \n" +
                "Made with 100% Angus Beef patty and top with\n" +
                "melted cheddar cheese, fresh lettuce, tomato, and\n" +
                "our secret sauce, this classic burger will leave you\n" +
                "wanting more. Served with crispy fries and a drink.\n" +
                "it's the perfect meal for any occasion.","fast_1",15,20,120,4));
        items.add(new FoodDomain("Pizza Peperoni","Pepperoni is an American variety of spicy salami made from cured pork and beef seasoned with paprika or other chili pepper. Prior to cooking, pepperoni is characteristically soft, slightly smoky, and bright red. Thinly sliced pepperoni is one of the most popular pizza toppings in American pizzerias"
                ,"fast_2",10,25,200,5));
        items.add(new FoodDomain("Vegetable Pizza","It's called “vegetariana”, sometimes “ortolana” (greengrocer). The topping is made with roasted or grilled peppers, zucchini, eggplants and red onions."
                ,"fast_3",13,30,100,4.5));

        recyclerViewFood = findViewById(R.id.view1);
        recyclerViewFood.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));

        adapterFoodList = new FoodListAdapter(items);
        recyclerViewFood.setAdapter(adapterFoodList);
    }
}