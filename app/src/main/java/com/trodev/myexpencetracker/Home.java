package com.trodev.myexpencetracker;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import com.example.finalproject.Activity.MainActivity;


public class Home extends AppCompatActivity {

    private Button foodDeliButton;
    private Button expenseTrackerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);


        foodDeliButton = findViewById(R.id.fooddeli_btn);
        expenseTrackerButton = findViewById(R.id.expensetracker_btn);


        foodDeliButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.example.finalproject");

                if (launchIntent != null){
                    startActivity(launchIntent);
                }else{
                    Toast.makeText(Home.this,"There is no package",Toast.LENGTH_LONG).show();
                }
            }
        });

        expenseTrackerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, MainActivityApp.class);
                startActivity(intent);
            }
        });
    }

}

