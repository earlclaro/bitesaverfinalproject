package com.trodev.myexpencetracker;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Wallet extends AppCompatActivity {

    private TextView userMoneyTextView;
    private EditText editWalletEditText;
    private Button editWalletButton;
    private Button addWalletButton;

    private double walletMoney = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wallet);

        userMoneyTextView = findViewById(R.id.user_money);
        editWalletEditText = findViewById(R.id.edit_wallet_edittext);
        editWalletButton = findViewById(R.id.edit_wallet_button);
        addWalletButton = findViewById(R.id.add_wallet_button);

        // Set initial wallet money value
        userMoneyTextView.setText("$ " + String.valueOf(walletMoney));

        editWalletButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String newAmountString = editWalletEditText.getText().toString().trim();
                if (!newAmountString.isEmpty()) {
                    double newAmount = Double.parseDouble(newAmountString);
                    walletMoney = newAmount;
                    userMoneyTextView.setText("$ " + String.valueOf(walletMoney));
                    editWalletEditText.setText("");
                }
            }
        });

        addWalletButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String additionalAmountString = editWalletEditText.getText().toString().trim();
                if (!additionalAmountString.isEmpty()) {
                    double additionalAmount = Double.parseDouble(additionalAmountString);
                    walletMoney += additionalAmount;
                    userMoneyTextView.setText("$ " + String.valueOf(walletMoney));
                    editWalletEditText.setText("");
                }
            }
        });
    }
}
